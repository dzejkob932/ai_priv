﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using testdll.API.Objects;
using testdll.API.Response.Companies;
using testdll.API.Response.Resources;
using testdll.API.Response.Transactions;
using testdll.TestDB;

namespace testdll.TestApiMethods
{
    class ResourcesMethods
    {
        public static async Task GetResources(int testParam, string token)
        {
            int userID = Program.user.Where(u => u.userToken == token).First().userId;
            List<ResourceModel> ret = new List<ResourceModel>();
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                
                string resp = "";
                using (var client = new WebClient())
                {
                    client.Headers.Add("Content-Type:application/json"); //Content-Type  
                    client.Headers.Add("Accept:application/json");
                    client.Headers.Add("Authorization", "Bearer " + token);
                    var result = await client.DownloadStringTaskAsync(GET_URLs.Resources).ConfigureAwait(true); //URI  
                   
                    resp = result;
                    GetUserResourcesResponseModel resources = new GetUserResourcesResponseModel();
                    resources = JsonConvert.DeserializeObject<GetUserResourcesResponseModel>(resp);

                   // ret.tests = new List<Test>();
                    ret = new List<ResourceModel>();
                    watch.Stop();
                    long TestTime = watch.ElapsedMilliseconds;
                    if (resources.execDetails.ExecTime == null || resources.execDetails.DbTime == null ||
                        TestTime == null)
                    {
                        resources.execDetails.ExecTime = 0;
                        resources.execDetails.DbTime = 0;
                        TestTime = 0;
                    }

                    //ret.tests = new Test(DateTime.Now, testParam, userID, (int) EndpointEnum.GetUserResources,
                    //    resources.execDetails.DbTime.Value, TestTime, resources.execDetails.ExecTime.Value);

                    ret.AddRange(resources.Resources);

                    Program.testsLis.Add(new Test(DateTime.Now, testParam, userID, (int)EndpointEnum.GetUserResources,
                        resources.execDetails.DbTime.Value, TestTime, resources.execDetails.ExecTime.Value));


                    Program.user.Where(u => u.userId == userID).ToList().ForEach(ug => ug.userResources = ret);

                    //   Program.user.Where(u => u.userId == userID).ToList().ForEach(ug => ug.userResources = ret.res);
                }
            }
            catch (Exception e)
            {
                //ret.tests = new List<Test>();

                //ret.tests = new Test(DateTime.Now, testParam, userID, (int) EndpointEnum.GetUserResources,
                //    0, 0, 0);

                Program.testsLis.Add(new Test(DateTime.Now, testParam, userID, (int)EndpointEnum.GetUserResources,
                    0, 0, 0));
            }
            //await Task.CompletedTask;
            //return Task.CompletedTask;
            //Console.WriteLine(Environment.NewLine + result);
            //return result;
        }
        }
    
}
