﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Mapping;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using testdll.API.Request;
using testdll.API.Response.Users;
using testdll.TestDB;

namespace testdll.TestApiMethods
{
    class UsersMethods
    {
        public static async Task<Test> RegisterUser(int testParam, string email, string password, string name)
        {
            
            List<Test> ret = new List<Test>();
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();


                var httpWebRequest = (HttpWebRequest)WebRequest.Create(POST_URLs.Register);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                RegisterRequest user = new RegisterRequest
                {
                    email = email,
                    password = password,
                    name = name
                };
                await using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string output = JsonConvert.SerializeObject(user);
                    await streamWriter.WriteAsync(output);
                    //streamWriter.Write(output);
                }

                string resp = "";
                var httpResponse = await httpWebRequest.GetResponseAsync();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = await streamReader.ReadToEndAsync();

                    resp = result;
                }
                RegisterResponse reg = new RegisterResponse();
                reg = JsonConvert.DeserializeObject<RegisterResponse>(resp);
                watch.Stop();
                long TestTime = watch.ElapsedMilliseconds;
                //ret.test = new List<Test>();
                if (reg.ExecDetails.ExecTime == null || reg.ExecDetails.DbTime == null || TestTime == null)
                {
                    reg.ExecDetails.ExecTime = 0;
                    reg.ExecDetails.DbTime = 0;
                    TestTime = 0;
                }

                //ret = new Test(DateTime.Now, testParam, 0, (int) EndpointEnum.AddUser,
                //    reg.ExecDetails.DbTime.Value, TestTime, reg.ExecDetails.ExecTime.Value);
                //Program.testsLis.Add(new Test(DateTime.Now, testParam, 0, (int)EndpointEnum.AddUser,
                //    reg.ExecDetails.DbTime.Value, TestTime, reg.ExecDetails.ExecTime.Value)); 
                
               Test test  = new Test(DateTime.Now, testParam, 0, (int)EndpointEnum.AddUser,
                    reg.ExecDetails.DbTime.Value, TestTime, reg.ExecDetails.ExecTime.Value);
               return test;
            }
            catch (Exception e)
            {

                //  ret.test = new Test(DateTime.Now, testParam, 0, (int)EndpointEnum.AddUser, 0, 0, 0);
                //Program.testsLis.Add(new Test(DateTime.Now, testParam, 0, (int)EndpointEnum.AddUser, 0, 0, 0));
                Test test = new Test(DateTime.Now, testParam, 0, (int)EndpointEnum.AddUser, 0, 0, 0);
                return test;
            }
            

            //return ret;
            //await Task.CompletedTask;
        }

        public static async Task<Test> GetUserId(int testParameters, string token)
        {
            int id = 0;
            double cash = 0;
            List<Test> returedId = new List<Test>();
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();

                string resp = "";
                using (var client = new WebClient())
                {
                    client.Headers.Add("Content-Type:application/json"); //Content-Type  
                    client.Headers.Add("Accept:application/json");
                    client.Headers.Add("Authorization", "Bearer " + token);
                    var result = await client.DownloadStringTaskAsync(GET_URLs.GetUserData).ConfigureAwait(true); //URI  
                    resp = result;
                    GetUserResponse user = new GetUserResponse();
                    user = JsonConvert.DeserializeObject<GetUserResponse>(resp);

                    id = user.user.Id;
                    cash = user.user.Cash;

                    watch.Stop();
                    long TestTime = watch.ElapsedMilliseconds;
                    if (user.execDetails.ExecTime == null || user.execDetails.DbTime == null || TestTime == null)
                    {
                        user.execDetails.ExecTime = 0;
                        user.execDetails.DbTime = 0;
                        TestTime = 0;
                    }

                    Program.user.Where(u => u.userToken == token).ToList().ForEach(ug => ug.userId = id);
                    Program.user.Where(u => u.userToken == token).ToList().ForEach(ug => ug.userCash = cash);


                    //List<UserGenerator> userGens = Program.user;
                    //foreach (var x in userGens)
                    //{
                    //    userGens.Where(u => u.userName == x.userName.ToString()).ToList().ForEach(ug => ug.userId = returedId.id);
                    //    userGens.Where(u => u.userName == x.userName.ToString()).ToList().ForEach(ug => ug.userCash = returedId.cash);
                    //}
                    //returedId.Add(new Test(DateTime.Now, testParameters, id, (int)EndpointEnum.GetUserInfo, user.execDetails.DbTime.Value, TestTime, user.execDetails.ExecTime.Value));
                    //Program.testsLis.Add(new Test(DateTime.Now, testParameters, id, (int)EndpointEnum.GetUserInfo, user.execDetails.DbTime.Value, TestTime, user.execDetails.ExecTime.Value));
                    Test test = new Test(DateTime.Now, testParameters, id, (int)EndpointEnum.GetUserInfo, user.execDetails.DbTime.Value, TestTime, user.execDetails.ExecTime.Value);
                    return test;

                }
            }
            catch (Exception e)
            {

                //returedId.test = new Test(DateTime.Now, testParameters, returedId.id, (int)EndpointEnum.GetUserInfo, 0, 0, 0);
                //Program.testsLis.Add(new Test(DateTime.Now, testParameters, id, (int)EndpointEnum.GetUserInfo, 0, 0, 0));
                Test test = new Test(DateTime.Now, testParameters, id, (int)EndpointEnum.GetUserInfo, 0, 0, 0);
                return test;
            }

            
            //await Task.CompletedTask;
            //return returedId;
        }

        public static async Task<Test> GetJWT(int testParam, string email, string password)
        {
            List<Test> ret = new List<Test>();
            string jwt = "";
            //try
            //{
            var watch = System.Diagnostics.Stopwatch.StartNew();

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(POST_URLs.Login);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            LoginRequest user = new LoginRequest();
            user.email = email;
            user.password = password;
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string output = JsonConvert.SerializeObject(user);
                await streamWriter.WriteAsync(output).ConfigureAwait(true);
                //streamWriter.Write(output);
            }

            string resp = "";
            var httpResponse = await httpWebRequest.GetResponseAsync().ConfigureAwait(true);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = await streamReader.ReadToEndAsync();
                resp = result;
            }

            LoginResponse login = new LoginResponse();
            login = JsonConvert.DeserializeObject<LoginResponse>(resp);

            jwt = login.jwt.ToString();

            watch.Stop();
            long TestTime = watch.ElapsedMilliseconds;
            if (login.ExecDetails.ExecTime == null || login.ExecDetails.DbTime == null || TestTime == null)
            {
                login.ExecDetails.ExecTime = 0;
                login.ExecDetails.DbTime = 0;
                TestTime = 0;
            }
            int userId = Program.user.Where(u => u.userEmail == email).First().userId;
            //List<UserGenerator> userGens = Program.user;

            Program.user.Where(u => u.userEmail == email).ToList().ForEach(ug => ug.userToken = jwt);


            //foreach (var x in userGens)
            //    {
            //        userGens.Where(u => u.userName == x.userName.ToString()).ToList()
            //            .ForEach(ug => ug.userToken = ret.jwt);
            //    }

            //ret.test = new Test(DateTime.Now, testParam, userId, (int)EndpointEnum.Login, login.ExecDetails.DbTime.Value, TestTime, login.ExecDetails.ExecTime.Value);

            //}
            //catch (Exception e)
            //{
            //    ret.testy = new List<Test>();
            //    ret.testy.Add(new Test(DateTime.Now, testParam, 0, (int)EndpointEnum.Login, 0, 0, 0));

            //}
            //if (ret.testy == null)
            //{
            //    ret.testy.Clear();
            //    ret.testy.Add(new Test(DateTime.Now, testParam, userId, (int)EndpointEnum.Login, 0, 0, 0));

            //}
            //Program.testsLis.Add(new Test(
            //    DateTime.Now, testParam, userId, (int)EndpointEnum.Login, login.ExecDetails.DbTime.Value, TestTime, login.ExecDetails.ExecTime.Value));
            Test test = new Test(
                DateTime.Now, testParam, 0, (int)EndpointEnum.Login, login.ExecDetails.DbTime.Value, TestTime, login.ExecDetails.ExecTime.Value);
            return test;
            //await Task.CompletedTask;
            //return ret.jwt;
        }

        public static async Task LogoutUser(int TestParamId, int id, string token)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                //ReturnLogout ret = new ReturnLogout();
                List<Test> ret = new List<Test>();
                string resp = "";
                using (var client = new WebClient())
                {
                    client.Headers.Add("Content-Type:application/json"); //Content-Type  
                    client.Headers.Add("Accept:application/json");
                    client.Headers.Add("Authorization", "Bearer " + token);
                    var result = await client.DownloadStringTaskAsync(GET_URLs.Logout).ConfigureAwait(true); //URI  
                    resp = result;
                    LogoutResponse logout = new LogoutResponse();
                    // respo user = new GetUserResponse();
                    logout = JsonConvert.DeserializeObject<LogoutResponse>(resp);


                    // ret.test = new List<Test>();
                    watch.Stop();
                    long TestTime = watch.ElapsedMilliseconds;
                    if (logout.ExecDetails.ExecTime == null || logout.ExecDetails.DbTime == null || TestTime == null)
                    {
                        logout.ExecDetails.ExecTime = 0;
                        logout.ExecDetails.DbTime = 0;
                        TestTime = 0;
                    }

                    ret.Add(new Test(DateTime.Now, TestParamId, id, (int)EndpointEnum.Logout, logout.ExecDetails.DbTime.Value,
                        TestTime,
                        logout.ExecDetails.ExecTime.Value));
                    Program.testsLis.Add(new Test(DateTime.Now, TestParamId, id, (int)EndpointEnum.Logout, logout.ExecDetails.DbTime.Value,
                        TestTime,
                        logout.ExecDetails.ExecTime.Value));

                }
            }
            catch (Exception e)
            {
                List<Test> ret = new List<Test>();
                //ret.Add(new Test(DateTime.Now, TestParamId, id, (int)EndpointEnum.Logout, 0,
                //    0,
                //    0));
                Program.testsLis.Add(new Test(DateTime.Now, TestParamId, id, (int)EndpointEnum.Logout, 0,
                    0,
                    0));

            }
            //await Task.CompletedTask;
            //return ret;

        }
    }
}
