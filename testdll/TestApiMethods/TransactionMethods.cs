﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using testdll.API.Objects;
using testdll.API.Response.Transactions;
using testdll.TestDB;

namespace testdll.TestApiMethods
{
    class TransactionMethods
    {
        public static async Task GetTransactions(int testParam, string token, int userID)
        {
            List<TransactionModel> ret = new List<TransactionModel>();
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();

                //GetCompaniesResponseModel comp = new GetCompaniesResponseModel();
                string resp = "";
                using (var client = new WebClient())
                {
                    client.Headers.Add("Content-Type:application/json"); //Content-Type  
                    client.Headers.Add("Accept:application/json");
                    client.Headers.Add("Authorization", "Bearer " + token);
                    var result = await client.DownloadStringTaskAsync(GET_URLs.Transactions).ConfigureAwait(true); //URI  
                    resp = result;
                    GetTransactionsByUserIdResponseModel transactions = new GetTransactionsByUserIdResponseModel();
                    transactions = JsonConvert.DeserializeObject<GetTransactionsByUserIdResponseModel>(resp);

                   // ret.tests = new List<Test>();
                    ret = new List<TransactionModel>();
                    watch.Stop();
                    long TestTime = watch.ElapsedMilliseconds;
                    if (transactions.ExecDetails.ExecTime == null || transactions.ExecDetails.DbTime == null ||
                        TestTime == null)
                    {
                        transactions.ExecDetails.ExecTime = 0;
                        transactions.ExecDetails.DbTime = 0;
                        TestTime = 0;
                    }

                    //ret.tests=new Test(DateTime.Now, testParam, userID, (int)EndpointEnum.GetTrasactions,
                    //    transactions.ExecDetails.DbTime.Value, TestTime, transactions.ExecDetails.ExecTime.Value);

                    ret.AddRange(transactions.Transactions);

                    Program.testsLis.Add(new Test(DateTime.Now, testParam, userID, (int)EndpointEnum.GetTrasactions,
                        transactions.ExecDetails.DbTime.Value, TestTime, transactions.ExecDetails.ExecTime.Value));


                    Program.user.Where(u => u.userId == userID).ToList().ForEach(ug => ug.userTransctions = ret);

                    //   Program.user.Where(u => u.userId == userID).ToList().ForEach(ug => ug.userResources = ret.res);
                }
            }
            catch (Exception e)
            {
                

                //ret.tests = new Test(DateTime.Now, testParam, userID, (int)EndpointEnum.GetTrasactions,
                //    0, 0, 0);

                Program.testsLis.Add(new Test(DateTime.Now, testParam, userID, (int)EndpointEnum.GetTrasactions,
                    0, 0, 0));
            }
            //await Task.CompletedTask;
            //  return ret;
            //Console.WriteLine(Environment.NewLine + result);
            //return result;
        }
    }
}
