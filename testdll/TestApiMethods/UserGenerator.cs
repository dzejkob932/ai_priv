﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testdll;
using testdll.API.Objects;
using testdll.TestApiMethods;
using testdll.TestDB;

namespace testdll.TestApiMethods
{
    public class UserGenerator
    {
        public string userName { get; set; }
        public string userPassword { get; set; }
        public string userEmail { get; set; }
        public string userToken { get; set; }
        public int userId { get; set; }
        public double userCash { get; set; }
        public List<ResourceModel> userResources { get; set; }
        public List<SellOfferModel> userSellOffers { get; set; }
        public List<BuyOfferModel> userBuyOffer { get; set; }
        public List<TransactionModel> userTransctions { get; set; }




        public UserGenerator(string name, string password, string email, string token, int id, double cash, List<ResourceModel> resources, List<SellOfferModel> sellOffer, List<BuyOfferModel> buyOffer, List<TransactionModel> transactions)
        {
            userName = name;
            userPassword = password;
            userEmail = email;
            userToken = token;
            userId = id;
            userCash = cash;
            userResources = resources;
            userSellOffers = sellOffer;
            userBuyOffer = buyOffer;
            userTransctions = transactions;

        }

        public static void CreateListOfUsers(int numOfUsers)
        {
            List<UserGenerator> userGens = new List<UserGenerator>();


            string name2 = Guid.NewGuid().ToString().Substring(0, 5);
            string name = "cccc";
            for (int i = 1; i < numOfUsers + 1; i++)
            {
                userGens.Add(new UserGenerator(name + i, "pass", name + i + "@user.user", "", 0, 0, null, null, null, null));
            }

            Program.user = userGens;
        }





    }


}

public class UGAction
{
    private UserGenerator _user = null;
    private TestParameters _testParams = null;
    
    public UGAction(UserGenerator user, TestParameters param)
    {
        _user = user;
        _testParams = param;
    }
    public async Task userGenerator()
    {

        try
        {

            List<Test> testy = new List<Test>();
            testy.Add(await UsersMethods.GetJWT(_testParams.TestParametersId, _user.userEmail, _user.userPassword));
            testy.Add(await UsersMethods.GetUserId(_testParams.TestParametersId, _user.userToken));
            int id = testy[testy.Count - 1].UserId;

            foreach (var test in testy)
            {
                
                Program.testsLis.Add(new Test(test.TestTime,test.TestParametersId,id,test.EndpointId,test.DatabaseTestTime,test.ApplicationTestTime,test.ApiTestTime));
            }

           

        }
        catch (Exception e)
        {
            List<Test> testy = new List<Test>();
            testy.Add(await UsersMethods.RegisterUser(_testParams.TestParametersId, _user.userEmail, _user.userPassword, _user.userName));
            testy.Add(await UsersMethods.GetJWT(_testParams.TestParametersId, _user.userEmail, _user.userPassword));
            testy.Add(await UsersMethods.GetUserId(_testParams.TestParametersId, _user.userToken));

            int id = testy[testy.Count - 1].UserId;

            foreach (var test in testy)
            {
                Program.testsLis.Add(new Test(test.TestTime,test.TestParametersId,id,test.EndpointId,test.DatabaseTestTime,test.ApplicationTestTime,test.ApiTestTime));
            }

           




        }
    }
}
