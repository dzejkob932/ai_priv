﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testdll.API.Objects;
using testdll.API.Response.ExecutingTimes;

namespace testdll.API.Response.BuyOffers
{
    public class GetBuyOffersByUserIdResponseModel
    {
        public List<BuyOfferModel> buyOffers { get; set; }
        public ExecutionDetails execDetails { get; set; }
    }
}
