﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testdll.API.Objects;
using testdll.API.Response.ExecutingTimes;

namespace testdll.API.Response.Users
{
    public class GetUserResponse
    {
        public UserModel user { get; set; }
        public ExecutionDetails execDetails { get; set; }
    }
}
