﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using testdll.API.Response.ExecutingTimes;

namespace testdll.API.Response.Users
{
    public class LogoutResponse
    {
        public ExecutionDetails ExecDetails { get; set; }
    }
}
