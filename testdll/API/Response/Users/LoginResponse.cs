﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testdll.API.Response.ExecutingTimes;

namespace testdll.API.Response.Users
{
    public class LoginResponse
    {
        /// <summary>
        /// Token
        /// </summary>
       // [JsonProperty("jwt")]
        public string jwt { get; set; }
        public ExecutionDetails ExecDetails { get; set; }
    }
}
