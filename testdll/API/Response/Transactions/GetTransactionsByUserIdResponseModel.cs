﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testdll.API.Objects;
using testdll.API.Response.ExecutingTimes;

namespace testdll.API.Response.Transactions
{
    public class GetTransactionsByUserIdResponseModel
    {
        public IList<TransactionModel> Transactions { get; set; }
        public ExecutionDetails ExecDetails { get; set; }
    }
}
