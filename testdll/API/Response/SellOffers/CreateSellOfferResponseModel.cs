﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testdll.API.Response.ExecutingTimes;

namespace testdll.API.Response.SellOffers
{
    public class CreateSellOfferResponseModel
    {
        public ExecutionDetails execDetails { get; set; }
    }
}
