﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testdll.API.Objects;
using testdll.API.Response.ExecutingTimes;

namespace testdll.API.Response.SellOffers
{
    public class GetSellOffersByUserIdResponseModel
    {
        public IList<SellOfferModel> SellOffers { get; set; }
        public ExecutionDetails execDetails { get; set; }
    }
}
