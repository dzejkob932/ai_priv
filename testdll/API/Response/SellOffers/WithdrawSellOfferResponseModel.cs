﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testdll.API.Response.ExecutingTimes;

namespace testdll.API.Response.SellOffers
{
    public class WithdrawSellOfferResponseModel
    {
        public ExecutionDetails execDetails { get; set; }
    }
}
