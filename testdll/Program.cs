﻿using System.Collections.Generic;
using System.Threading.Tasks;
using testdll.API.Objects;
using testdll.TestApiMethods;
using testdll.TestDB;
using Serilog;
using System;
using System.Linq;
using System.Reflection;

namespace testdll
{
    class Program
    {
        public static List<Test> testsLis = new List<Test>();
        public static List<CompanyModel> comp = new List<CompanyModel>();
        public static List<UserGenerator> user = new List<UserGenerator>();
        public static List<TestParameters> ListTestParameterses = new List<TestParameters>();
        public static List<string> listaAkcji = new List<string>{"Logowanie z pobraniem informacji o userze"};

        public static void fillListParamList(int liczba)
        {
            for (int i = 0; i <= liczba; i++)
            {
                ListTestParameterses.Add(new TestParameters(i, i.ToString(), i + 29, 4, 20, 40, 10, 30));
            }
        }

        public static TestParameters GetTestParameters(int numberOfParameters)
        {
            fillListParamList(numberOfParameters);
            return ListTestParameterses[numberOfParameters];
        }




        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .WriteTo.File("logs\\.txt", rollingInterval: RollingInterval.Minute)
                .CreateLogger();



            RunFirst(1);

            Log.Information("Lista akcji:");

            foreach (var u in listaAkcji)
            {
                Log.Information(u);
            }

            //foreach (var u in user)
            //{
            //    Log.Information(u.userToken);
            //}


            foreach (var test in testsLis)
            {
                if (test != null)
                {
                    Log.Information(test.TestTime + "." + test.TestTime.Millisecond + ", " + test.TestParametersId + ", " + test.TestId + ", " + test.UserId + ", " + test.EndpointId + ", " + test.ApiTestTime + ", " + test.DatabaseTestTime + ", " + test.ApplicationTestTime);

                    //Log.Information("TimeStamp: " + test.TestTime + "." + test.TestTime.Millisecond);
                    //Log.Information("TestParam: " + test.TestParametersId);
                    //Log.Information("TestId: " + test.TestId);
                    //Log.Information("UserId: " + test.UserId);
                    //Log.Information("EndpointId: " + test.EndpointId);
                    //Log.Information("API_Time: " + test.ApiTestTime / 1000);
                    //Log.Information("DB_Time: " + test.DatabaseTestTime / 1000);
                    //Log.Information("Apl_Time: " + test.ApplicationTestTime);
                    //Console.WriteLine("done");
                    //Log.Debug("");
                }
                else
                {
                    Log.Error("NULL");
                    Log.Error("NULL");
                    Log.Error("NULL");
                }
            }
            //foreach (var c in Program.comp)
            //{

            //    Log.Information("Company:   " + c.Id + "   " + c.Name);

            //}
            Log.CloseAndFlush();
        }











        public static async Task RunFirst(int testParameterId)
        {

            var watch = System.Diagnostics.Stopwatch.StartNew();

            //TestParameters testParams = new TestParameters(666, "x", 10, 10, 10, 20, 10, 20);
            TestParameters testParams = GetTestParameters(testParameterId);

            UserGenerator.CreateListOfUsers(testParams.NumberOfUsers);
            List<UserGenerator> ug = Program.user;

            List<Task> generateUserList = new List<Task>();

            for (int i = 0; i < ug.Count; i++)
            {
                generateUserList.Add(new UGAction(ug[i], testParams).userGenerator());
            }

            Task.WaitAll(generateUserList.ToArray());

            watch.Stop();
            long TestTime = watch.ElapsedMilliseconds;
            Log.Information(TestTime.ToString());

            //RepairLoginTestData();


            var watch2 = System.Diagnostics.Stopwatch.StartNew();
            List<Task> userTasks = new List<Task>();

            var reflection = typeof(UserActions);
            List<string> listOfMethods = reflection.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(method => method.IsPrivate).Select(method => method.Name).ToList();
            int indexWylogowywanie = listOfMethods.IndexOf("Wylogowanie");
            listOfMethods.RemoveAt(indexWylogowywanie);
            Random r = new Random();
            List<string> randomMethodorder = new List<string>();
            int j = 0;
            while (j < testParams.NumberOfRequests)
            {
                int number = r.Next(listOfMethods.Count);
                randomMethodorder.Add(listOfMethods[number]);
                j++;
                //listOfMethods.RemoveAt(number);
            }
            randomMethodorder.Add("Wylogowanie");
            for (int i = 0; i < ug.Count; i++)
            {
                //userTasks.Add(new UserActions(ug[i], testParams).PerformUserAction());
                userTasks.Add(new UserActions(ug[i], testParams).Random(randomMethodorder));
            }

            Task.WaitAll(userTasks.ToArray());
            listaAkcji.AddRange(randomMethodorder);


            watch2.Stop();
            long TestTime2 = watch2.ElapsedMilliseconds;
            Log.Information(TestTime2.ToString());


            // RepairFinishTests(paramId, testId);

        }



        //public static void RepairLoginTestData()
        //{
        //    List<Test> listaTestow = Program.testsLis;

        //    for (int i = listaTestow.Count - 1; i >= 0; --i)
        //    {
        //        if (listaTestow[i].UserId == 0)
        //        {
        //            listaTestow[i].UserId = listaTestow[i + 1].UserId;
        //        }
        //    }
        //}

        //public static void RepairFinishTests(int paramId, int testId)
        //{
        //    List<Test> listaTestow = Program.testsLis;

        //    for (int i = 0; i < listaTestow.Count; i++)
        //    {
        //        if (listaTestow[i] == null)
        //        {
        //            listaTestow.RemoveAt(listaTestow.IndexOf(listaTestow[i]));
        //            listaTestow.Add(new Test(DateTime.Now, 0, 0, 0, 0, 0, 0));
        //        }
        //    }

        //    foreach (var test in listaTestow)
        //    {
        //        test.TestId = testId;
        //        test.TestParametersId = paramId;



        //    }
        //}

    }

    public class UserActions
    {
        //private List<ResourceModel> _resources = new List<ResourceModel>();
        private UserGenerator _user = null;
        private TestParameters _test = null;
        public UserActions(UserGenerator user, TestParameters test)
        {
            _user = user;
            _test = test;
        }

        public async Task PerformUserAction()
        {

            await WyswietlanieOfertKupna();
            await DodanieFirmy();
            await NowaOfertaSprzedazy();
            await WyswietlanieOfertSprzedazy();
            await NowaOfertaKupna();
            await WycofanieOfertyKupna();
            await WyswietlanieTransakcji();
            await WycofanieOfertySprzedazy();
            await WyswietlanieZasobów();


            //await DodanieFirmy();
            //await NowaOfertaKupna();
            //await WyswietlanieOfertKupna();

        }

        public async Task Random(List<string> methods)
        {
            foreach (var method in methods)
            {
                await (Task)this.GetType().GetMethod(method, BindingFlags.NonPublic | BindingFlags.Instance).Invoke(this, null);
            }
        }





        private async Task WyswietlanieOfertKupna()
        {
            await BuyOfferMethods.GetUserBuyOffers(_test.TestParametersId, _user.userId, _user.userToken);
        }
        private async Task NowaOfertaKupna()
        {
            await CompaniesMethod.GetCompanies(_test.TestParametersId, _user.userToken, _user.userId);
            await BuyOfferMethods.AddBuyOffer(_test.TestParametersId, _user.userToken, _user.userId, _test.MinBuyPrice, _test.MaxBuyPrice);
        }
        private async Task WycofanieOfertyKupna()
        {
            await BuyOfferMethods.GetUserBuyOffers(_test.TestParametersId, _user.userId, _user.userToken);
            await BuyOfferMethods.PutBuyOffers(_test.TestParametersId, _user.userToken, _user.userId);
            await BuyOfferMethods.GetUserBuyOffers(_test.TestParametersId, _user.userId, _user.userToken);
        }

        private async Task DodanieFirmy()
        {
            await CompaniesMethod.POSTCompanies(_user.userId, _test.TestParametersId, _user.userToken);
            await ResourcesMethods.GetResources(_test.TestParametersId, _user.userToken);
        }
        private async Task WyswietlanieTransakcji()
        {
            await TransactionMethods.GetTransactions(_test.TestParametersId, _user.userToken, _user.userId);
        }
        private async Task WyswietlanieZasobów()
        {
            await ResourcesMethods.GetResources(_test.TestParametersId, _user.userToken);
        }
        private async Task WyswietlanieOfertSprzedazy()
        {
            await SellOffersMethods.GetUserSellOffers(_test.TestParametersId, _user.userToken);
        }
        private async Task NowaOfertaSprzedazy()
        {
            await ResourcesMethods.GetResources(_test.TestParametersId, _user.userToken);
            await SellOffersMethods.AddSellOffer(_test.TestParametersId, _user.userToken, 20, 50);
            await SellOffersMethods.GetUserSellOffers(_test.TestParametersId, _user.userToken);
        }

        private async Task WycofanieOfertySprzedazy()
        {
            await SellOffersMethods.GetUserSellOffers(_test.TestParametersId, _user.userToken);
            await SellOffersMethods.PutSellOffers(_test.TestParametersId, _user.userToken, _user.userId);
        }
        private async Task Wylogowanie()
        {
            await UsersMethods.LogoutUser(_test.TestParametersId, _user.userId, _user.userToken);
        }


    }




}